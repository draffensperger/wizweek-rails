(function() {
  'use strict';

  angular
    .module('tasks')
    .controller('TasksController', TasksController);

  /** @ngInject */
  function TasksController() {
    var vm = this;
    vm.tasks = [{ name: 'do something1' }];
  }
})();
