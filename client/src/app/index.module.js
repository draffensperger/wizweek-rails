(function() {
  'use strict';

  angular
    .module('tasks', ['satellizer', 'ngRoute']);
})();
