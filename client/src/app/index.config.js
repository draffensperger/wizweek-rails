(function() {
  'use strict';

  angular
    .module('tasks')
    .config(config);

  /** @ngInject */
  function config($authProvider, $logProvider, $windowProvider) {
    var $window = $windowProvider.$get();

    $authProvider.google({
      clientId: '944131974902-59t0ekqanbv7krguorbl3c0fk5rosika.apps.googleusercontent.com',
      redirectUri: $window.location.origin + '/v1/auth/google/callback'
    });

    // Enable log
    $logProvider.debugEnabled(true);
  }
})();
