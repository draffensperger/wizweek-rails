(function() {
  'use strict';

  angular
    .module('tasks')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($auth) {
    var vm = this;

    vm.authenticate = function(provider) {
      $auth.authenticate(provider);
    };
  }
})();
