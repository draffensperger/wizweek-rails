(function() {
  'use strict';

  angular
    .module('tasks')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/tasks', {
        templateUrl: 'app/tasks/tasks.html',
        controller: 'TasksController',
        controllerAs: 'tasks'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
