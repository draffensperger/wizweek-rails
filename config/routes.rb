Rails.application.routes.draw do
  devise_for :users
  api_version(module: 'V1', path: { value: 'v1' }) do
    resources :tasks, except: [:new, :edit]
    get '/auth/:provider/callback', to: 'sessions#create'
  end
end
