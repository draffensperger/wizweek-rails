# Model for a task
class Task < ActiveRecord::Base
  validates :name, presence: true
end
